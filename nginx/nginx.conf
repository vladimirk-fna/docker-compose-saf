user                    nginx;
error_log               /var/log/nginx/error.log warn;
pid                     /var/run/nginx.pid;
worker_processes        auto;
worker_rlimit_nofile    18528;

events {
    worker_connections  1024;
}

http {
    default_type  application/octet-stream;
    client_max_body_size 0;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

    map $http_upgrade $connection_upgrade {
        default     "upgrade";
    }

    server {
        listen        80 default_server;
        return 301 https://$host$request_uri;
    }

    server {
        listen 443 ssl;
        server_name  localhost;
        error_page  497 https://$host$request_uri;

        ssl_certificate    /etc/nginx/ssl/certificate.crt;
        ssl_certificate_key    /etc/nginx/ssl/server.key;
    
        error_log     /var/log/nginx/error.log;
        access_log    /var/log/nginx/access.log;
 
        ssl_session_timeout  5m;
        ssl_protocols  TLSv1.1 TLSv1.2;
        ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
        ssl_prefer_server_ciphers   on;

        location / {
            proxy_pass             http://fna-web:8080;
            proxy_connect_timeout 159s;
            proxy_send_timeout 120;
            proxy_read_timeout 120;
            proxy_set_header    Connection         $connection_upgrade;
            proxy_set_header    Upgrade            $http_upgrade;
            proxy_set_header    Host               $host;
            proxy_set_header    X-Real-IP          $remote_addr;
            proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Proto  $scheme;
        }
    }
}