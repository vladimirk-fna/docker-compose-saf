FROM openjdk:8u151-jdk-alpine
MAINTAINER hhs@fna.fi
ADD fna-web-*.war /fna.war
ADD entrypoint.sh /
ADD *.cer /fnalab-local.cer
RUN echo yes | keytool -import -alias fnalab -file fnalab-local.cer -keystore \
$JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt
RUN 	apk update && \
apk add bash && \
chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
