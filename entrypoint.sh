#!/bin/bash
export _JAVA_OPTIONS=" \
-Xms${XMS} \
-Xmx${XMX} \
-Dfna.url=${FNA_URL} \
-XX:+UseConcMarkSweepGC \
-Dserver.jsp-servlet.init-parameters.mappedfile=false \
-Dserver.tomcat.protocolHeader=X-Forwarded-Proto \
-Dserver.tomcat.remoteIpHeader=X-Forwarded-For \
-Dmail.smtp.starttls.enable=${SMTP_STARTTLS_ENABLE} \
-Dmail.smtp.auth=${SMTP_AUTH} \
-Dfna.upload.maxSize=16777216000 \
-Dfna.core.engine=${FNA_CORE_ENGINE} \
-Dfna.env=${FNA_ENV} \
-Dmail.host=${MAIL_HOST} \
-Dmail.port=${MAIL_PORT} \
-Dmail.username=${MAIL_USERNAME} \
-Dmail.password=${MAIL_PASSWORD} \
-Dmailchimp.lists.interested.id=${MAILCHIMP_INTRESTED_ID} \
-Dmailchimp.lists.promotions.id=${MAILCHIMP_PROMOTIONS_ID} \
-Dmailchimp.lists.subscribers.id=${MAILCHIMP_SUBSCRIBERS_ID} \
-Dmailchimp.lists.users.id=${MAILCHIMP_USERS_ID} \
-Dmandrill.apikey=${MANDRILL_APIKEY} \
-Dcontext.checker.timer=30000 \
-Dcontext.not.running.time=3600000 \
-Dstatic.fnacore.files.base.dir=${STATIC_BASE_DIR} \
-Djdbc.url=${FNA_JDBC} \
-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE} \
-Dsaml.idp.metadata=${SAML_IDP_METADATA} \
-Dldap.domain=${LDAP_DOMAIN} \
-Dldap.url=${LDAP_URL} \
-Dldap.group=${LDAP_GROUP}"


exec  java  -jar /fna.war